import React from 'react'
import ReactDOM from 'react-dom'
import { FavoriteNumber } from '../favorite-number'

test('renders a number input with a label "Favorite Number"', () => {
  const container = document.createElement('div')
  ReactDOM.render(<FavoriteNumber />, container)
  expect(container.querySelector('input').type).toBe('number')
  expect(container.querySelector('label')).toHaveTextContent(/favorite number/i)
})
