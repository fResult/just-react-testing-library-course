import React from 'react'
import { render } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { FavoriteNumber } from '../favorite-number'

test('entering an invalid value shows an error message', () => {
  const screen = render(<FavoriteNumber />)
  const input = screen.getByLabelText(/favorite number/i)
  userEvent.type(input, '10')

  expect(screen.getByRole('alert')).toHaveTextContent(/the number is invalid/i)
})
