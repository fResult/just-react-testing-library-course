const config = require('kcd-scripts/jest')

/** @type { import('@jest/types/build/Config').InitialOptions } */
module.exports = {
  ...config,
  // we have no coverageThreshold on this project...
  coverageThreshold: {},
}
